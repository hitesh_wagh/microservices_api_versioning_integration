package com.mindstix.demo;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mindstix.annotation.AutoApiVersion;

@AutoApiVersion("1.5")
@RestController
@RequestMapping("/users")
public class A {
    
    @RequestMapping(method = RequestMethod.GET, value = "/records", produces = "application/json")
    public void sampleApi() {
        
    }

}
